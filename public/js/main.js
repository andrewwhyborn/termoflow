$(document).ready(function () {
    $("#home-slider").owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: false,
        nav: true,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        dots: true,
    });
    
    $('.news-slider').owlCarousel({
        nav: true,
        dots: false,
        margin: 15,
        navText: ['<div class="nav"><i class="fa fa-angle-left" aria-hidden="true"></i></div>', '<div class="nav"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            1024: {
                items: 3,
            }
        }
    });


    $('.photo-slider').owlCarousel({
        nav: false,
        dots: true,
        margin: 15,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            1024: {
                items: 3,
            }
        }
    });

    const top = $(window).scrollTop();
    window.scrollTo(0, top + 1);
    window.scrollTo(0, top -1);

    $(".search-container>button").click(function(){
        $(".search-container").toggleClass("active");
    });
});

var inputs = document.querySelectorAll('.inputfile');
Array.prototype.forEach.call(inputs, function (input) {
    var label = input.nextElementSibling,
        labelVal = label.innerHTML;
    input.addEventListener('change', function (e) {
        var fileName = '';
        if (this.files && this.files.length > 1)
            fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
            fileName = e.target.value.split('\\').pop();
        if (fileName)
            label.querySelector('.filename').innerHTML = fileName;
        else
            label.innerHTML = labelVal;
    });
});

document.addEventListener(
    "DOMContentLoaded", () => {
        Mmenu = new Mmenu("#menu", {
            navbar: {
                title: 'Termoflow'
            },
            navbars: [
                {
                    position: "top",
                    content: [
                        "<div id=\"close-menu\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>"
                    ]
                }
            ]
        }, {
            offCanvas: {
                clone: true
            },
        });

        $('.mm-navbar__btn .mm-sronly').html('Назад');
        $('.mm-navbar').click(function(e){
            Mmenu.API.close();
        })
    }
);